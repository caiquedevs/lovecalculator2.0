import { ChangeEvent, useState } from "react";
import { Container, Form } from "./styled";
import "./app.css";

interface IResponse {
  _id: string;
  id: string;
  percents: number;
  createdAt: Date;
  updatedAt: Date;
  __v: number;
}

function App() {
  const [nameOne, setNameOne] = useState("");
  const [nameTwo, setNameTwo] = useState("");
  const [loading, setLoading] = useState(false);
  const [percents, setPercents] = useState(0);

  const handleChangeNameOne = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    setNameOne(value);
  };

  const handleChangeNameTwo = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    setNameTwo(value);
  };

  const calculator = (value: number) => {
    let count = 0;
    const time = setInterval(() => {
      if (count === value) {
        clearInterval(time);
        setLoading(false);
        return;
      }

      count += 1;
      setPercents(count);
    }, 25);
  };

  const fetchData = async () => {
    const names = nameOne + nameTwo;
    const data = names.replace(/\s/g, "").toLowerCase();

    const rawResponse = await fetch(
      `${process.env.REACT_APP_URL}/posts/${data}`
    );

    const content: IResponse = await rawResponse.json();
    calculator(content.percents);
  };

  const handleSubmit = async (formEvent: any) => {
    formEvent.preventDefault();
    setLoading(true);
    fetchData();
  };

  const handleClickClear = () => {
    setPercents(0);
    setNameOne("");
    setNameTwo("");
  };

  return (
    <Container className="App">
      <Form onSubmit={handleSubmit}>
        <figure>
          <img src="./heart.png" alt="heart" />
          <span>{percents}%</span>
          <figcaption>Porcentagem de compatibilidade</figcaption>
        </figure>

        <input
          type="text"
          onChange={handleChangeNameOne}
          value={nameOne}
          placeholder="Seu nome"
        />
        <input
          type="text"
          onChange={handleChangeNameTwo}
          value={nameTwo}
          placeholder="Nome do pretendente"
        />

        <button disabled={loading || !nameOne || !nameTwo} type="submit">
          Calcular
        </button>

        {percents !== 0 && !loading && (
          <button
            onClick={handleClickClear}
            className="btn-clear"
            type="button"
          >
            Limpar
          </button>
        )}
      </Form>
    </Container>
  );
}

export default App;
